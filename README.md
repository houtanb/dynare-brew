# Houtanb Dynare-brew

## How do I install these formulae?
`brew install houtanb/dynare-brew/<formula>`

Or `brew tap houtanb/dynare-brew` and then `brew install <formula>`.

Or install via URL (which will not receive updates):

```
brew install https://raw.githubusercontent.com/houtanb/homebrew-dynare-brew/master/Formula/<formula>.rb
```

## Documentation
`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
